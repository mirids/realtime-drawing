const events = {
    setNickname: "setNickname",
    newUser: "newUser",
    disconnect: "disconnect",
    disconnected: "disconnected",
    sendMsg: "sendMsg",
    newMsg: "newMsg",
    beginPath: "beginPath",
    strokePath: "strokePath",
    beganPath: "beganPath",
    strokedPath: "strokedPath",
    fill: "fill",
    filled: "filled",
    playerUpdate: "playerUpdate",
    gameStarting: "gameStarting",
    gameStarted: "gameStarted",
    gameEnded: "gameEnded",
    leaderNotification: "leaderNotification"

}

export default events;