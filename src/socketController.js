import events from './events';
import { chooseWord } from './words';
import { start } from 'repl';

let sockets = [];
let inProgress = false;
let word = null;
let leader = null;
let timeout = null;

const choseLeader = () => sockets[Math.floor(Math.random() * sockets.length)];

const socketController = (socket, io) => {
    const broadcast = (event, data) => socket.broadcast.emit(event, data);
    const superBroadcast = (event, data) => io.emit(event, data);
    const sendPlayerUpdate = () =>
      superBroadcast(events.playerUpdate, { sockets });
    const startGame = () => {
        if(sockets.length > 1) {
            if(inProgress === false) {
                console.log('inprogress');
                inProgress = true;
                leader = choseLeader();
                word = chooseWord();
                superBroadcast(events.gameStarting);
                setTimeout(() => {
                    superBroadcast(events.gameStarted);
                    io.to(leader.id).emit(events.leaderNotification, { word });
                    timeout = setTimeout(endGame(), 30000);
                }, 3000);   
            }
        }
    }

    const endGame = () => {
        inProgress = false;
        superBroadcast(events.gameEnded);
        if(timeout !== null) {
            clearTimeout(timeout);
        }
        setTimeout(() => startGame(), 2000);        
    }

    const addPoints = (id) => {
        sockets = sockets.map(socket => {
            if(socket.id === id) {
                socket.points += 10;
            }
            return socket;
        });
        sendPlayerUpdate();
    }

    socket.on(events.setNickname, ({ nickname }) => {
        socket.nickname = nickname;
        sockets.push({id: socket.id, points: 0, nickname: nickname});
        broadcast(events.newUser, { nickname });
        sendPlayerUpdate();
        startGame();
    });

    socket.on(events.disconnect, () => {
        sockets = sockets.filter(aSocket => aSocket.id !== socket.id);
        broadcast(events.disconnected, { nickname: socket.nickname });
        sendPlayerUpdate();
        if(sockets.length === 1) {
            endGame();
        } else if(leader){
            if(socket.id === leader.id) {
                endGame();
            }
        }
    });

    socket.on(events.sendMsg, ({ message }) => {
        if(message === word) {
            superBroadcast(events.newMsg, {message: `The winner is: ${socket.nickname}, word was: ${word}`, nickname: 'Bot'});
            addPoints(socket.id);
        } else {
            broadcast(events.newMsg, { message, nickname: socket.nickname });
        }
    });

    socket.on(events.beginPath, ({ x, y, color}) => 
        broadcast(events.beganPath, { x, y, color})
    );

    socket.on(events.strokePath, ({ x, y, color }) => 
        broadcast(events.strokedPath, { x, y, color})
    );

    socket.on(events.fill, ({ color }) => {
        broadcast(events.filled, ({ color }));
    });
}

export default socketController;